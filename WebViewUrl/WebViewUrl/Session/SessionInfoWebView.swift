//
//  SessionInfoWebView.swift
//  WebViewUrl
//
//  Created by Александр on 18.02.2020.
//  Copyright © 2020 Александр. All rights reserved.
//

import Foundation

final public class SessionInfoWebView {
    
    private var stringParse: [String] = []
    
    public init () {
        
    }
    
    public func infoWebView(char: ReadChar, url: URL, complition: @escaping (Result<ModelWebView, Error>) -> Void) {
        let defaultSession = URLSession(configuration: .default)
        var dataTask: URLSessionDataTask?
        dataTask?.cancel()
            dataTask = defaultSession.dataTask(with: url) { data, response, error in
                defer { dataTask = nil }
                if let error = error {
                    print("DEBUG ", error)
                    complition(.error(error))
                } else if let data = data,
                    let response = response as? HTTPURLResponse,
                    response.statusCode == 200 {
                    self.stringParse.removeAll()
                    let string = String(bytes: data, encoding: .utf8)
                    var stringNext = ""
                    if stringNext.isEmpty {
                        stringNext = string ?? "0"
                    }
                    let strings = stringNext.components(separatedBy: char.satrt)
                    
                    if strings.count == 4 {
                        let newStringsArray = [strings[1], strings[2], strings[3]]
                        for item in newStringsArray {
                            guard let stringFinal = item.components(separatedBy: char.end).first else {
                                complition(.error(CustomError.parceError))
                                return
                            }
                            self.stringParse.append(stringFinal)
                        }
                    } else {
                        complition(.error(CustomError.parceError))
                    }
                    guard let model = self.convertStringToModel(strings: self.stringParse) else {
                        complition(.error(CustomError.parceError))
                        return
                    }
                    complition(.success(model))
                }
            }
            dataTask?.resume()
    }
    
    private func convertStringToModel(strings: [String]) -> ModelWebView? {
        var model: ModelWebView?
        
        var date: Date?
        var state: SatetWebView?
        var url: URL?
        
        for item in strings  {
            if let dateCustom = self.dateFormator(string: item) {
                date = dateCustom
            } else if let stateCustom = self.stateConverter(string: item) {
                state = stateCustom
            } else if let urlCustom = self.createUrl(string: item) {
                url = urlCustom
            }
        }
        
        if date != nil && state != nil && url != nil {
            model = ModelWebView(date: date!, isEnabled: state!.satus, url: url!)
        }
        
        return model
    }
    
    private func dateFormator(string: String) -> Date? {
        let formatter = DateFormatter()
        formatter.dateFormat = "dd.MM.yyyy"
        formatter.timeZone = TimeZone(abbreviation: "UTC")
        let dateWebView = formatter.date(from: string)
        return dateWebView
    }
    
    private func stateConverter(string: String) -> SatetWebView? {
        return SatetWebView(rawValue: string.lowercased())
    }
    
    private func createUrl(string: String) -> URL? {
        return URL(string: "http://\(string)")
    }
    
}

enum CustomError: Error {
    case parceError
}

enum SatetWebView: String {
    case on
    case off
    
    var satus: Bool {
        switch self {
        case .on:
            return true
        case .off:
            return false
        }
    }
}

