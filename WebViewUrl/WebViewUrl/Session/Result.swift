//
//  Result.swift
//  WebViewUrl
//
//  Created by Александр on 18.02.2020.
//  Copyright © 2020 Александр. All rights reserved.
//

public enum Result<S,E: Error> {
    case success(S)
    case error(E)
}
