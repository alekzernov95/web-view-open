//
//  ModelWebView.swift
//  WebViewUrl
//
//  Created by Александр on 18.02.2020.
//  Copyright © 2020 Александр. All rights reserved.
//

import Foundation

public struct ModelWebView {
    public let date: Date
    public let isEnabled: Bool
    public let url: URL
    
    public init (date: Date, isEnabled: Bool, url: URL) {
        self.date = date
        self.isEnabled = isEnabled
        self.url = url
    }
}
