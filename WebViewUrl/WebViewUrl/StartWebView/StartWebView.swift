//
//  StartWebView.swift
//  WebViewUrl
//
//  Created by Александр on 18.02.2020.
//  Copyright © 2020 Александр. All rights reserved.
//

import UIKit

final public class InvoiceWebView {
    
    var window: UIWindow?
    var startViewController: UIViewController?
    let url: URL
    
    public static let defChar = ReadChar(satrt: "<i>", end: "</i>")
    let webView = SessionInfoWebView()
    let alertHelper = CreateAlert()
    let char: ReadChar
    
    public init(window: UIWindow?, startViewController: UIViewController?, url: URL, char: ReadChar = InvoiceWebView.defChar) {
        self.window = window
        self.startViewController = startViewController
        self.url = url
        self.char = char
    }
    
    
    public func showWebView(isStart: Bool = true) {
        
  self.webView.infoWebView(char: self.char, url: self.url) { result in
            DispatchQueue.main.sync {
                switch result {
                case .success(let value):
                    if value.isEnabled && self.isDateActive(value.date) {
                        let webViewViewController = WebViewController()
                        webViewViewController.setUrl(url: value.url)
                        self.window?.topViewController?.present(webViewViewController, animated: true, completion: nil)
                    } else if isStart {
                        self.window?.rootViewController = self.startViewController
                    }
                case .error:
                    let alert = self.alertHelper.alerInfo()
                    self.alertHelper.click = { [weak self] in
                        self?.showWebView()
                    }
                    self.window?.topViewController?.present(alert, animated: true, completion: nil)
                }
            }
        }
        
    }
    
    
    private func isDateActive(_ date: Date) -> Bool {
        let formatter = DateFormatter()
        formatter.dateFormat = "dd-MM-yyyy"
        formatter.timeZone = TimeZone(abbreviation: "UTC")
        let now = Date()
        let dateString = formatter.string(from:now)
        let dateNow = formatter.date(from: dateString)!
        if dateNow == date || dateNow > date {
            return true
        } else {
            return false
        }
    }
    
}

public extension UIWindow {
    
    var topViewController: UIViewController? {
        var top = self.rootViewController
        while true {
            if let presented = top?.presentedViewController {
                top = presented
            } else if let nav = top as? UINavigationController {
                top = nav.visibleViewController
            } else if let tab = top as? UITabBarController {
                top = tab.selectedViewController
            } else {
                break
            }
        }
        return top
    }
    
}
