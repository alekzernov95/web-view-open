//
//  AlertView.swift
//  WebViewUrl
//
//  Created by Александр on 18.02.2020.
//  Copyright © 2020 Александр. All rights reserved.
//

import UIKit

public final class CreateAlert {
    
    public init () {
        
    }
    
    public var click: (() -> (Void))?
    
    public func alerInfo() -> UIAlertController  {
        let alert = UIAlertController(title: "", message: "No internet Connection", preferredStyle: .alert)
        let buttonRefrash = UIAlertAction(title: "Refresh", style: .default, handler: { _ in
            self.click?()
        })
        alert.addAction(buttonRefrash)
        return alert
    }
    
}
