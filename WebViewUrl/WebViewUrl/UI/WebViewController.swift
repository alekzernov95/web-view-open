//
//  WebViewController.swift
//  WebViewUrl
//
//  Created by Александр on 18.02.2020.
//  Copyright © 2020 Александр. All rights reserved.
//

import UIKit
import WebKit

final public class WebViewController: UIViewController {
    
    private var webView = WKWebView()
    private var url: URL!
    
    override public func viewDidLoad() {
        super.viewDidLoad()
        self.view.addSubview(self.webView)
        self.webView.load(URLRequest(url: url))
    }
    
    override public func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        
        
        let viewWidth = self.view.frame.width
        
        self.webView.frame.size.width = viewWidth
        self.webView.frame.size.height = self.view.frame.height
        
    }
    
    public func setUrl(url: URL) {
        self.url = url
    }
    
}
